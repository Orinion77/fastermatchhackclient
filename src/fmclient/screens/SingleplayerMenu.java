package fmclient.screens;

import fmclient.main.CustomPanel;
import fmclient.main.View;
import fmlibrary.Settings;

@SuppressWarnings("serial")
public class SingleplayerMenu extends CustomPanel
{
    public SingleplayerMenu(View pView)
    {
        initComponents();
        initPanel(pView, "17;11;8;8;14;9;9;10;7;33;27;27;28;28;29;29;30;30;31;31;32;32",
                  backToMainMenuButton, digitCountLabel, easyLabel, easyLabel1, gameModeLabel, hardLabel, hardLabel1, operatorCountLabel, startGameButton, timeLabel,
                  oneLabel1, oneLabel2, twoLabel1, twoLabel2, threeLabel1, threeLabel2, fourLabel1, fourLabel2, fiveLabel1, fiveLabel2, sixLabel1, sixLabel2);

        operatorCountSlider.setValue(Integer.valueOf(Settings.get("singleplayerOperatorCount")));
        digitCountSlider.setValue(Integer.valueOf(Settings.get("singleplayerDigitCount")));
        timeTextField.setText(Settings.get("singleplayerTime"));

        gameModeComboBox.setSelectedItem(Settings.get("singleplayerMode"));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        startGameButton = new javax.swing.JButton();
        digitCountSlider = new javax.swing.JSlider();
        easyLabel = new javax.swing.JLabel();
        hardLabel = new javax.swing.JLabel();
        digitCountLabel = new javax.swing.JLabel();
        twoLabel1 = new javax.swing.JLabel();
        operatorCountSlider = new javax.swing.JSlider();
        threeLabel1 = new javax.swing.JLabel();
        easyLabel1 = new javax.swing.JLabel();
        fourLabel1 = new javax.swing.JLabel();
        sixLabel1 = new javax.swing.JLabel();
        fiveLabel1 = new javax.swing.JLabel();
        operatorCountLabel = new javax.swing.JLabel();
        hardLabel1 = new javax.swing.JLabel();
        oneLabel1 = new javax.swing.JLabel();
        gameModeComboBox = new javax.swing.JComboBox<>();
        gameModeLabel = new javax.swing.JLabel();
        backToMainMenuButton = new javax.swing.JButton();
        twoLabel2 = new javax.swing.JLabel();
        threeLabel2 = new javax.swing.JLabel();
        fourLabel2 = new javax.swing.JLabel();
        sixLabel2 = new javax.swing.JLabel();
        fiveLabel2 = new javax.swing.JLabel();
        oneLabel2 = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        timeTextField = new javax.swing.JTextField();

        setBackground(new java.awt.Color(0, 0, 0));

        startGameButton.setBackground(new java.awt.Color(200, 188, 174));
        startGameButton.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        startGameButton.setText("L7");
        startGameButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                startGameButtonActionPerformed(evt);
            }
        });

        digitCountSlider.setBackground(new java.awt.Color(0, 0, 0));
        digitCountSlider.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        digitCountSlider.setMaximum(6);
        digitCountSlider.setMinimum(1);
        digitCountSlider.setValue(0);

        easyLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        easyLabel.setForeground(new java.awt.Color(200, 188, 174));
        easyLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        easyLabel.setText("L8");

        hardLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        hardLabel.setForeground(new java.awt.Color(200, 188, 174));
        hardLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        hardLabel.setText("L9");

        digitCountLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        digitCountLabel.setForeground(new java.awt.Color(200, 188, 174));
        digitCountLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        digitCountLabel.setText("L11");

        twoLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        twoLabel1.setForeground(new java.awt.Color(200, 188, 174));
        twoLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        twoLabel1.setText("L28");

        operatorCountSlider.setBackground(new java.awt.Color(0, 0, 0));
        operatorCountSlider.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        operatorCountSlider.setMaximum(7);
        operatorCountSlider.setMinimum(2);
        operatorCountSlider.setValue(2);

        threeLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        threeLabel1.setForeground(new java.awt.Color(200, 188, 174));
        threeLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        threeLabel1.setText("L29");

        easyLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        easyLabel1.setForeground(new java.awt.Color(200, 188, 174));
        easyLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        easyLabel1.setText("L8");

        fourLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        fourLabel1.setForeground(new java.awt.Color(200, 188, 174));
        fourLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        fourLabel1.setText("L30");

        sixLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        sixLabel1.setForeground(new java.awt.Color(200, 188, 174));
        sixLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sixLabel1.setText("L32");

        fiveLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        fiveLabel1.setForeground(new java.awt.Color(200, 188, 174));
        fiveLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        fiveLabel1.setText("L31");

        operatorCountLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        operatorCountLabel.setForeground(new java.awt.Color(200, 188, 174));
        operatorCountLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        operatorCountLabel.setText("L10");

        hardLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        hardLabel1.setForeground(new java.awt.Color(200, 188, 174));
        hardLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        hardLabel1.setText("L9");

        oneLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        oneLabel1.setForeground(new java.awt.Color(200, 188, 174));
        oneLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        oneLabel1.setText("L27");

        gameModeComboBox.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        gameModeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Standard" }));

        gameModeLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        gameModeLabel.setForeground(new java.awt.Color(200, 188, 174));
        gameModeLabel.setText("L14");

        backToMainMenuButton.setBackground(new java.awt.Color(200, 188, 174));
        backToMainMenuButton.setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        backToMainMenuButton.setText("L17");
        backToMainMenuButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                backToMainMenuButtonActionPerformed(evt);
            }
        });

        twoLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        twoLabel2.setForeground(new java.awt.Color(200, 188, 174));
        twoLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        twoLabel2.setText("L28");

        threeLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        threeLabel2.setForeground(new java.awt.Color(200, 188, 174));
        threeLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        threeLabel2.setText("L29");

        fourLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        fourLabel2.setForeground(new java.awt.Color(200, 188, 174));
        fourLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        fourLabel2.setText("L30");

        sixLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        sixLabel2.setForeground(new java.awt.Color(200, 188, 174));
        sixLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sixLabel2.setText("L32");

        fiveLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        fiveLabel2.setForeground(new java.awt.Color(200, 188, 174));
        fiveLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        fiveLabel2.setText("L31");

        oneLabel2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        oneLabel2.setForeground(new java.awt.Color(200, 188, 174));
        oneLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        oneLabel2.setText("L27");

        timeLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        timeLabel.setForeground(new java.awt.Color(200, 188, 174));
        timeLabel.setText("L33");

        timeTextField.setBackground(new java.awt.Color(200, 188, 174));
        timeTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(easyLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(gameModeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(gameModeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addComponent(operatorCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(operatorCountSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(hardLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(oneLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(twoLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(threeLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(fourLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(fiveLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(sixLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(easyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(digitCountSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(hardLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(digitCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(timeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(oneLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(twoLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(threeLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(fourLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(fiveLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(sixLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(97, 97, 97)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(startGameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(backToMainMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(gameModeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(gameModeLabel))
                .addGap(18, 18, 18)
                .addComponent(operatorCountLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(operatorCountSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(easyLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(oneLabel1)
                            .addComponent(twoLabel1)
                            .addComponent(threeLabel1)
                            .addComponent(fourLabel1)
                            .addComponent(fiveLabel1)
                            .addComponent(sixLabel1)))
                    .addComponent(hardLabel1))
                .addGap(31, 31, 31)
                .addComponent(digitCountLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hardLabel)
                    .addComponent(digitCountSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(easyLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(oneLabel2)
                    .addComponent(twoLabel2)
                    .addComponent(threeLabel2)
                    .addComponent(fourLabel2)
                    .addComponent(fiveLabel2)
                    .addComponent(sixLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(timeLabel)
                    .addComponent(timeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(startGameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(backToMainMenuButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void startGameButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_startGameButtonActionPerformed
    {//GEN-HEADEREND:event_startGameButtonActionPerformed
        Settings.set("singleplayerMode", String.valueOf(gameModeComboBox.getSelectedItem()));
        Settings.set("singleplayerOperatorCount", String.valueOf(operatorCountSlider.getValue()));
        Settings.set("singleplayerDigitCount", String.valueOf(digitCountSlider.getValue()));
        Settings.set("singleplayerTime", timeTextField.getText());
        changeScreen(new Gamescreen(view));
    }//GEN-LAST:event_startGameButtonActionPerformed

    private void backToMainMenuButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_backToMainMenuButtonActionPerformed
    {//GEN-HEADEREND:event_backToMainMenuButtonActionPerformed
        changeScreen(new MainMenu(view));
    }//GEN-LAST:event_backToMainMenuButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backToMainMenuButton;
    private javax.swing.JLabel digitCountLabel;
    private javax.swing.JSlider digitCountSlider;
    private javax.swing.JLabel easyLabel;
    private javax.swing.JLabel easyLabel1;
    private javax.swing.JLabel fiveLabel1;
    private javax.swing.JLabel fiveLabel2;
    private javax.swing.JLabel fourLabel1;
    private javax.swing.JLabel fourLabel2;
    private javax.swing.JComboBox<String> gameModeComboBox;
    private javax.swing.JLabel gameModeLabel;
    private javax.swing.JLabel hardLabel;
    private javax.swing.JLabel hardLabel1;
    private javax.swing.JLabel oneLabel1;
    private javax.swing.JLabel oneLabel2;
    private javax.swing.JLabel operatorCountLabel;
    private javax.swing.JSlider operatorCountSlider;
    private javax.swing.JLabel sixLabel1;
    private javax.swing.JLabel sixLabel2;
    private javax.swing.JButton startGameButton;
    private javax.swing.JLabel threeLabel1;
    private javax.swing.JLabel threeLabel2;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JTextField timeTextField;
    private javax.swing.JLabel twoLabel1;
    private javax.swing.JLabel twoLabel2;
    // End of variables declaration//GEN-END:variables
}
