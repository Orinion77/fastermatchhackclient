package fmclient.screens;

import fmclient.main.CustomPanel;
import fmclient.main.View;
import fmlibrary.Language;
import fmlibrary.Settings;

@SuppressWarnings("serial")
public class SettingsMenu extends CustomPanel
{
    public SettingsMenu(View pView)
    {
        initComponents();
        initPanel(pView, "4;19;20;18", backButton, musicCheckBox, soundCheckBox, languagePackageLabel);

        musicCheckBox.setSelected(Boolean.parseBoolean(Settings.get("music")));
        soundCheckBox.setSelected(Boolean.parseBoolean(Settings.get("sound")));

        setLanguagePackages();
    }

    private void setLanguagePackages()
    {
        String[] packs = Language.getLanguageFiles();
        for(String pack : packs)
        {
            languagePackComboBox.addItem(pack.replace(".language", ""));
        }
        languagePackComboBox.setSelectedItem(Settings.get("language").replace(".language", ""));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        backButton = new javax.swing.JButton();
        languagePackageLabel = new javax.swing.JLabel();
        languagePackComboBox = new javax.swing.JComboBox<String>();
        musicCheckBox = new javax.swing.JCheckBox();
        soundCheckBox = new javax.swing.JCheckBox();

        setBackground(new java.awt.Color(0, 0, 0));

        backButton.setBackground(new java.awt.Color(200, 188, 174));
        backButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        backButton.setText("L4");
        backButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                backButtonActionPerformed(evt);
            }
        });

        languagePackageLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        languagePackageLabel.setForeground(new java.awt.Color(200, 188, 174));
        languagePackageLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        languagePackageLabel.setText("L18");

        languagePackComboBox.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N

        musicCheckBox.setBackground(new java.awt.Color(0, 0, 0));
        musicCheckBox.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        musicCheckBox.setForeground(new java.awt.Color(200, 188, 174));
        musicCheckBox.setSelected(true);
        musicCheckBox.setText("L19");
        musicCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        soundCheckBox.setBackground(new java.awt.Color(0, 0, 0));
        soundCheckBox.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        soundCheckBox.setForeground(new java.awt.Color(200, 188, 174));
        soundCheckBox.setSelected(true);
        soundCheckBox.setText("L20");
        soundCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(languagePackComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(languagePackageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(musicCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(backButton, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                            .addComponent(soundCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(48, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(languagePackageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(languagePackComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(musicCheckBox)
                    .addComponent(soundCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(backButton)
                .addGap(24, 24, 24))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_backButtonActionPerformed
    {//GEN-HEADEREND:event_backButtonActionPerformed
        Settings.set("language", String.valueOf(languagePackComboBox.getSelectedItem() + ".language"));
        Settings.set("music", String.valueOf(musicCheckBox.isSelected()));
        Settings.set("sound", String.valueOf(soundCheckBox.isSelected()));
        changeScreen(new MainMenu(view));
    }//GEN-LAST:event_backButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JComboBox<String> languagePackComboBox;
    private javax.swing.JLabel languagePackageLabel;
    private javax.swing.JCheckBox musicCheckBox;
    private javax.swing.JCheckBox soundCheckBox;
    // End of variables declaration//GEN-END:variables
}
