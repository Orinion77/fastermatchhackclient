package fmclient.main;

import fmlibrary.FigureValidator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JToggleButton;

public class FigureDisplayListenerMulti implements ActionListener
{
    private FigureDisplay display;

    public FigureDisplayListenerMulti(FigureDisplay pDisplay)
    {
        this.display = pDisplay;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        JToggleButton source = (JToggleButton)e.getSource();
        for(int j = 0; j < display.getButtons().length; j++)
        {
            if(display.getButtons()[j].equals(source))
            {
                if(source.isSelected())
                {
                    if(FigureValidator.isValidAddPosition(display.getFigure(), j)
                       && (display.getMultiplayerGamesreen().getState() == GameState.ONE_MATCH_LEFT
                           || (display.getMultiplayerGamesreen().getState() == GameState.ONE_MATCH_FROM_LAST_FIGURE_LEFT
                               && display.getMultiplayerGamesreen().getLastFigureChanged().equals(display))))
                    {
                        display.getFigure().setPosition(j, !display.getFigure().getPosition(j));
                        display.getMultiplayerGamesreen().setState(GameState.NO_MATCH_LEFT);
                        display.getMultiplayerGamesreen().setLastFigureChanged(display);
                    }
                    else
                    {
                        source.setSelected(!source.isSelected());
                    }
                }
                else if(display.getMultiplayerGamesreen().getState() == GameState.NO_MATCH_LEFT)
                {
                    if(FigureValidator.isValidRemovePosition(display.getFigure(), j))
                    {
                        display.getFigure().setPosition(j, !display.getFigure().getPosition(j));
                        display.getMultiplayerGamesreen().setState(GameState.ONE_MATCH_LEFT);
                    }
                    else
                    {
                        display.getFigure().setPosition(j, !display.getFigure().getPosition(j));
                        display.getMultiplayerGamesreen().setState(GameState.ONE_MATCH_FROM_LAST_FIGURE_LEFT);
                    }

                    display.getMultiplayerGamesreen().setLastFigureChanged(display);
                }
                else
                {
                    source.setSelected(!source.isSelected());
                }
                return;
            }
        }
    }
}
