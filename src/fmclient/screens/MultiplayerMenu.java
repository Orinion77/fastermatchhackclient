package fmclient.screens;

import fmclient.main.*;
import static fmlibrary.ProtocolType.LOGIN;
import static fmlibrary.ProtocolType.LOGOUT;
import fmlibrary.Settings;
import javax.swing.*;

@SuppressWarnings("serial")
public class MultiplayerMenu extends CustomPanel
{
    private FMClient Client;
    private View view;

    public MultiplayerMenu(View pView)
    {
        initComponents();
        initPanel(pView, "17;24;22;23;25;26", backToMainMenuButton, loginButton, usernameLabel, lobbyLabel, serverIPLabel, portLabel);

        serverIPTextField.setText(Settings.get("serverIP"));
        portTextField.setText(Settings.get("port"));
        usernameTextField.setText(Settings.get("username"));
        view = pView;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        loginButton = new javax.swing.JButton();
        backToMainMenuButton = new javax.swing.JButton();
        serverIPTextField = new javax.swing.JTextField();
        serverIPLabel = new javax.swing.JLabel();
        portLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        usernameTextField = new javax.swing.JTextField();
        portTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        lobbyList = new javax.swing.JList<String>();
        lobbyLabel = new javax.swing.JLabel();
        numberOfUserLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 0, 0));

        loginButton.setBackground(new java.awt.Color(200, 188, 174));
        loginButton.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        loginButton.setText("L24");
        loginButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                loginButtonActionPerformed(evt);
            }
        });

        backToMainMenuButton.setBackground(new java.awt.Color(200, 188, 174));
        backToMainMenuButton.setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        backToMainMenuButton.setText("L17");
        backToMainMenuButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                backToMainMenuButtonActionPerformed(evt);
            }
        });

        serverIPTextField.setBackground(new java.awt.Color(200, 188, 174));
        serverIPTextField.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        serverIPTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        serverIPLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        serverIPLabel.setForeground(new java.awt.Color(200, 188, 174));
        serverIPLabel.setText("L25");

        portLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        portLabel.setForeground(new java.awt.Color(200, 188, 174));
        portLabel.setText("L26");

        usernameLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        usernameLabel.setForeground(new java.awt.Color(200, 188, 174));
        usernameLabel.setText("L22");

        usernameTextField.setBackground(new java.awt.Color(200, 188, 174));
        usernameTextField.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        usernameTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        portTextField.setBackground(new java.awt.Color(200, 188, 174));
        portTextField.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        portTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        lobbyList.setBackground(new java.awt.Color(200, 188, 174));
        lobbyList.setModel(new DefaultListModel());
        lobbyList.setEnabled(false);
        lobbyList.setFocusable(false);
        jScrollPane1.setViewportView(lobbyList);

        lobbyLabel.setForeground(new java.awt.Color(200, 188, 174));
        lobbyLabel.setText("L23");

        numberOfUserLabel.setForeground(new java.awt.Color(200, 188, 174));
        numberOfUserLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        numberOfUserLabel.setText("0 / 0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 43, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lobbyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(78, 78, 78)
                        .addComponent(numberOfUserLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(portLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(serverIPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(usernameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(usernameTextField)
                            .addComponent(serverIPTextField)
                            .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(44, 44, 44))
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(backToMainMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serverIPLabel)
                    .addComponent(serverIPTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(portLabel)
                    .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameLabel)
                    .addComponent(usernameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lobbyLabel)
                    .addComponent(numberOfUserLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(backToMainMenuButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_loginButtonActionPerformed
    {//GEN-HEADEREND:event_loginButtonActionPerformed
        Settings.set("serverIP", serverIPTextField.getText());
        Settings.set("port", portTextField.getText());
        Settings.set("username", usernameTextField.getText());
        Client = new FMClient(Settings.get("serverIP"), Integer.valueOf(Settings.get("port")), this);
        Client.send(LOGIN, Settings.get("username"));
        loginButton.setEnabled(false);
        serverIPTextField.setEditable(false);
        portTextField.setEditable(false);
        usernameTextField.setEditable(false);
    }//GEN-LAST:event_loginButtonActionPerformed

    private void backToMainMenuButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_backToMainMenuButtonActionPerformed
    {//GEN-HEADEREND:event_backToMainMenuButtonActionPerformed
        Client.send(LOGOUT, "");
        changeScreen(new MainMenu(view));
    }//GEN-LAST:event_backToMainMenuButtonActionPerformed

    public JList<String> getList()
    {
        return lobbyList;
    }

    public JLabel getnumberOfUsersLabel()
    {
        return numberOfUserLabel;
    }

    public View getView()
    {
        return view;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backToMainMenuButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lobbyLabel;
    private javax.swing.JList<String> lobbyList;
    private javax.swing.JButton loginButton;
    private javax.swing.JLabel numberOfUserLabel;
    private javax.swing.JLabel portLabel;
    private javax.swing.JTextField portTextField;
    private javax.swing.JLabel serverIPLabel;
    private javax.swing.JTextField serverIPTextField;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JTextField usernameTextField;
    // End of variables declaration//GEN-END:variables
}
