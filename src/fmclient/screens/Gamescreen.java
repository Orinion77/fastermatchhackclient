package fmclient.screens;

import fmclient.main.*;
import fmlibrary.*;
import fmlibrary.List;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Gamescreen extends CustomPanel
{
    private static final double FIGURE_GAP = 2.5;
    private static final double FIGURE_POS_X = 10;
    private static final double FIGURE_WIDTH = 50;

    private Timer timer;
    private List figures;
    private GameState state;
    private Task task;
    private FigureDisplay lastFigureChanged;

    public Gamescreen(View pView)
    {
        initComponents();
        initPanel(pView, "5;34;36", leftTimeLabel, matchesLeftlabel, finishedLabel);

        state = GameState.NO_MATCH_LEFT;

        task = GameGenerator.generateTask(Integer.valueOf(Settings.get("singleplayerOperatorCount")), Integer.valueOf(Settings.get("singleplayerDigitCount")));
        figures = task.getTask();

        displayFigures(task.getTask(), 5);

        startTimer(Integer.valueOf(Settings.get("singleplayerTime")));
    }

    private void startTimer(int maxTimeInSec)
    {
        timeProgressBar.setMaximum(maxTimeInSec * 2);
        double factor = timeProgressBar.getMaximum() / maxTimeInSec;
        timer = new Timer(500, (ActionEvent e)
                          ->
                          {
                              timeProgressBar.setValue(timeProgressBar.getValue() + 1);
                              leftTimeNumbersLabel.setText("" + ((timeProgressBar.getMaximum() - timeProgressBar.getValue()) / factor));

                              if(timeProgressBar.getValue() >= timeProgressBar.getMaximum())
                              {
                                  timer.stop();

                                  String fileName = Settings.get("language");
                                  matchesLeftlabel.setText(Language.getTranslation(fileName, "38"));

                                  displayFigures(task.getSolution(), 120);

                                  Util.doAfterTime(5000, ()
                                                   ->
                                                   {
                                                       changeScreen(new Gamescreen(view));
                                  });
                              }
                });
        timer.start();
    }

    private void displayFigures(List pFigures, double pFiguresPosY)
    {
        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();

        double screenWidth = screensize.width - 5;

        double figuresPosX = FIGURE_POS_X;
        double figureWidth = FIGURE_WIDTH;
        double figuresGap = FIGURE_GAP;

        int figuresCount = Util.listSize(pFigures);

        double totalWidth = figuresPosX + figureWidth * (figuresCount + 1) + figuresGap * (figuresCount - 1) + 100;
        if(totalWidth > screenWidth)
        {
            double factor = screenWidth / totalWidth;
            totalWidth = screenWidth;
            figuresGap *= factor;
            figureWidth *= factor;
            figuresPosX *= factor;
        }
        else if(totalWidth < 580)
            totalWidth = 580;

        pFigures.toFirst();
        while(pFigures.hasAccess())
        {
            FigureDisplay figureDisplay = new FigureDisplay(this, (int)figuresPosX, (int)pFiguresPosY, (int)figureWidth, (Figure)pFigures.getObject());
            figuresPosX += figureWidth + figuresGap;
            pFigures.next();
        }

        setPreferredSize(new Dimension((int)totalWidth, (int)getPreferredSize().getHeight()));
        view.pack();
        view.repaint();
    }

    public void setState(GameState state)
    {
        String fileName = Settings.get("language");
        if(state == GameState.ONE_MATCH_LEFT || state == GameState.ONE_MATCH_FROM_LAST_FIGURE_LEFT)
            matchesLeftlabel.setText(Language.getTranslation(fileName, "35"));
        else
        {
            matchesLeftlabel.setText(Language.getTranslation(fileName, "34"));
            if(GameGenerator.isFinished(figures))
            {
                timer.stop();

                matchesLeftlabel.setText(Language.getTranslation(fileName, "38"));
                finishedLabel.setText(Language.getTranslation(fileName, "37"));
                finishedLabel.setForeground(Color.GREEN);

                displayFigures(task.getSolution(), 120);

                Util.doAfterTime(5000, ()
                                 ->
                                 {
                                     changeScreen(new Gamescreen(view));
                        });

            }
            else
            {
                finishedLabel.setText(Language.getTranslation(fileName, "36"));
                finishedLabel.setForeground(Color.RED);
            }
        }
        this.state = state;
    }

    public Timer getTimer()
    {
        return timer;
    }

    public void setLastFigureChanged(FigureDisplay lastFigureChanged)
    {
        this.lastFigureChanged = lastFigureChanged;
    }

    public FigureDisplay getLastFigureChanged()
    {
        return lastFigureChanged;
    }

    public GameState getState()
    {
        return state;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pauseMenuButton = new javax.swing.JButton();
        leftTimeLabel = new javax.swing.JLabel();
        leftTimeNumbersLabel = new javax.swing.JLabel();
        timeProgressBar = new javax.swing.JProgressBar();
        finishedLabel = new javax.swing.JLabel();
        matchesLeftlabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 0, 0));

        pauseMenuButton.setBackground(new java.awt.Color(200, 188, 174));
        pauseMenuButton.setText("| |");
        pauseMenuButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                pauseMenuButtonActionPerformed(evt);
            }
        });

        leftTimeLabel.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        leftTimeLabel.setForeground(new java.awt.Color(200, 188, 174));
        leftTimeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        leftTimeLabel.setText("L5");

        leftTimeNumbersLabel.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        leftTimeNumbersLabel.setForeground(new java.awt.Color(200, 188, 174));
        leftTimeNumbersLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        leftTimeNumbersLabel.setText("0");

        timeProgressBar.setToolTipText("");

        finishedLabel.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        finishedLabel.setForeground(new java.awt.Color(255, 0, 0));
        finishedLabel.setText("L36/L37");

        matchesLeftlabel.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        matchesLeftlabel.setForeground(new java.awt.Color(255, 153, 51));
        matchesLeftlabel.setText("L34/L35");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(timeProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pauseMenuButton))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(matchesLeftlabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(finishedLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(leftTimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(leftTimeNumbersLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pauseMenuButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 192, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(leftTimeNumbersLabel)
                    .addComponent(leftTimeLabel)
                    .addComponent(matchesLeftlabel)
                    .addComponent(finishedLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timeProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pauseMenuButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_pauseMenuButtonActionPerformed
    {//GEN-HEADEREND:event_pauseMenuButtonActionPerformed
        changeScreen(new PauseMenu(view, this));
    }//GEN-LAST:event_pauseMenuButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel finishedLabel;
    private javax.swing.JLabel leftTimeLabel;
    private javax.swing.JLabel leftTimeNumbersLabel;
    private javax.swing.JLabel matchesLeftlabel;
    private javax.swing.JButton pauseMenuButton;
    private javax.swing.JProgressBar timeProgressBar;
    // End of variables declaration//GEN-END:variables
}
