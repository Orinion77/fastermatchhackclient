package fmclient.main;

import fmlibrary.Language;
import fmlibrary.Settings;
import javax.swing.*;

@SuppressWarnings("serial")
public abstract class CustomPanel extends JPanel
{
    protected View view;

    public void initPanel(View pView, String pTranslationIdentifiers, JComponent... pComponents)
    {
        view = pView;
        setComponentsText(pTranslationIdentifiers, pComponents);
    }

    protected void changeScreen(CustomPanel pCustomPanel)
    {
        view.setContentPane(pCustomPanel);
        view.pack();
    }

    private void setComponentsText(String pTranslationIdentifiers, JComponent... pComponents)
    {
        String fileName = Settings.get("language");
        String[] translationIdentifiers = pTranslationIdentifiers.split(";");

        for(int i = 0; i < pComponents.length; i++)
        {
            if(pComponents[i] instanceof JLabel)
                ((JLabel)pComponents[i]).setText(Language.getTranslation(fileName, translationIdentifiers[i]));
            if(pComponents[i] instanceof JButton)
                ((AbstractButton)pComponents[i]).setText(Language.getTranslation(fileName, translationIdentifiers[i]));
        }
    }
}
