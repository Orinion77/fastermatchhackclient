package fmclient.screens;

import fmclient.main.CustomPanel;
import fmclient.main.View;

@SuppressWarnings("serial")
public class PauseMenu extends CustomPanel
{
    private Gamescreen gamescreen;

    public PauseMenu(View pView, Gamescreen pGamescreen)
    {
        initComponents();
        initPanel(pView, "13;12", endGameButton, continueGameButton);

        gamescreen = pGamescreen;
        gamescreen.getTimer().stop();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        continueGameButton = new javax.swing.JButton();
        endGameButton = new javax.swing.JButton();
        logoLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 0, 0));

        continueGameButton.setBackground(new java.awt.Color(200, 188, 174));
        continueGameButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        continueGameButton.setText("L12");
        continueGameButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                continueGameButtonActionPerformed(evt);
            }
        });

        endGameButton.setBackground(new java.awt.Color(200, 188, 174));
        endGameButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        endGameButton.setText("L13");
        endGameButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                endGameButtonActionPerformed(evt);
            }
        });

        logoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fmclient/screens/FMBlackWhite450.jpg"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(logoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(endGameButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(continueGameButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(73, 73, 73)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(logoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(continueGameButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(endGameButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void continueGameButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_continueGameButtonActionPerformed
    {//GEN-HEADEREND:event_continueGameButtonActionPerformed
        changeScreen(gamescreen);
        gamescreen.getTimer().start();
    }//GEN-LAST:event_continueGameButtonActionPerformed

    private void endGameButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_endGameButtonActionPerformed
    {//GEN-HEADEREND:event_endGameButtonActionPerformed
        changeScreen(new MainMenu(view));
    }//GEN-LAST:event_endGameButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton continueGameButton;
    private javax.swing.JButton endGameButton;
    private javax.swing.JLabel logoLabel;
    // End of variables declaration//GEN-END:variables
}
