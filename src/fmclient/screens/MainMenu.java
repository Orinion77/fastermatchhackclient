package fmclient.screens;

import fmclient.main.CustomPanel;
import fmclient.main.View;

@SuppressWarnings("serial")
public class MainMenu extends CustomPanel
{
    public MainMenu(View pView)
    {
        initComponents();
        initPanel(pView, "2;3;1;21", multiplayerMenuButton, creditsButton, singleplayerMenuButton, settingsButton);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        singleplayerMenuButton = new javax.swing.JButton();
        multiplayerMenuButton = new javax.swing.JButton();
        creditsButton = new javax.swing.JButton();
        logoLabel = new javax.swing.JLabel();
        settingsButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 0, 0));

        singleplayerMenuButton.setBackground(new java.awt.Color(200, 188, 174));
        singleplayerMenuButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        singleplayerMenuButton.setText("L1");
        singleplayerMenuButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                singleplayerMenuButtonActionPerformed(evt);
            }
        });

        multiplayerMenuButton.setBackground(new java.awt.Color(200, 188, 174));
        multiplayerMenuButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        multiplayerMenuButton.setText("L2");
        multiplayerMenuButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                multiplayerMenuButtonActionPerformed(evt);
            }
        });

        creditsButton.setBackground(new java.awt.Color(200, 188, 174));
        creditsButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        creditsButton.setText("L3");
        creditsButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                creditsButtonActionPerformed(evt);
            }
        });

        logoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fmclient/screens/FMBlackWhite450.jpg"))); // NOI18N

        settingsButton.setBackground(new java.awt.Color(200, 188, 174));
        settingsButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        settingsButton.setText("L21");
        settingsButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                settingsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(logoLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(singleplayerMenuButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(multiplayerMenuButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(settingsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(creditsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(singleplayerMenuButton)
                        .addGap(30, 30, 30)
                        .addComponent(multiplayerMenuButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(settingsButton)
                        .addGap(18, 18, 18)
                        .addComponent(creditsButton)
                        .addGap(13, 13, 13))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(logoLabel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void singleplayerMenuButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_singleplayerMenuButtonActionPerformed
    {//GEN-HEADEREND:event_singleplayerMenuButtonActionPerformed
        changeScreen(new SingleplayerMenu(view));
    }//GEN-LAST:event_singleplayerMenuButtonActionPerformed

    private void multiplayerMenuButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_multiplayerMenuButtonActionPerformed
    {//GEN-HEADEREND:event_multiplayerMenuButtonActionPerformed
        changeScreen(new MultiplayerMenu(view));
    }//GEN-LAST:event_multiplayerMenuButtonActionPerformed

    private void settingsButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_settingsButtonActionPerformed
    {//GEN-HEADEREND:event_settingsButtonActionPerformed
        changeScreen(new SettingsMenu(view));
    }//GEN-LAST:event_settingsButtonActionPerformed

    private void creditsButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_creditsButtonActionPerformed
    {//GEN-HEADEREND:event_creditsButtonActionPerformed
        changeScreen(new Credits(view));
    }//GEN-LAST:event_creditsButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton creditsButton;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JButton multiplayerMenuButton;
    private javax.swing.JButton settingsButton;
    private javax.swing.JButton singleplayerMenuButton;
    // End of variables declaration//GEN-END:variables
}
