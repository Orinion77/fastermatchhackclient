package fmclient.screens;

import fmclient.main.CustomPanel;
import fmclient.main.View;

@SuppressWarnings("serial")
public class Credits extends CustomPanel
{
    public Credits(View pView)
    {
        initComponents();
        initPanel(pView, "17", backButton);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        backButton = new javax.swing.JButton();
        creditsLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 0, 0));

        backButton.setBackground(new java.awt.Color(200, 188, 174));
        backButton.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        backButton.setText("L4");
        backButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                backButtonActionPerformed(evt);
            }
        });

        creditsLabel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        creditsLabel.setForeground(new java.awt.Color(200, 188, 174));
        creditsLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        creditsLabel.setText("<html><center>Created by:<br><br>Christopher Zinda<br>Felix Borgelt<br>Andreas Pingel<br><br>_________________________<br><br>Inspired by:<br>M.Kersting<center></html>");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(118, 118, 118)
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(129, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(creditsLabel)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(creditsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(backButton)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_backButtonActionPerformed
    {//GEN-HEADEREND:event_backButtonActionPerformed
        changeScreen(new MainMenu(view));
    }//GEN-LAST:event_backButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JLabel creditsLabel;
    // End of variables declaration//GEN-END:variables
}
