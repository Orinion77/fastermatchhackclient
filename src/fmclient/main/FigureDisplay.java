package fmclient.main;

import fmclient.screens.Gamescreen;
import fmclient.screens.MultiplayerGamesreen;
import fmlibrary.Figure;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.JToggleButton;
import javax.swing.plaf.metal.MetalToggleButtonUI;

public class FigureDisplay
{
    private static final double ASPECT_RATIO = 4.5;

    private Figure figure;
    private Gamescreen gamescreen;
    private MultiplayerGamesreen multigamescreen;
    private JToggleButton[] buttons;

    public FigureDisplay(Gamescreen pGamescreen, int pX, int pY, int pTotalWidth, Figure pFigure)
    {
        figure = pFigure;
        gamescreen = pGamescreen;

        int buttonHeight = (int)(pTotalWidth / (2 + ASPECT_RATIO));
        int buttonWidth = (int)(ASPECT_RATIO * buttonHeight);

        switch(figure.getFigureType())
        {
            case NUMBER:
                createNumber(pX, pY, buttonWidth, buttonHeight);
                break;
            case PLUSMINUS:
                createPlusMin(pX, pY, buttonWidth, buttonHeight);
                break;
            case EQUAL:
                createEqual(pX, pY, buttonWidth, buttonHeight);
                break;
        }
    }

    public FigureDisplay(MultiplayerGamesreen pGamescreen, int pX, int pY, int pTotalWidth, Figure pFigure)
    {
        figure = pFigure;
        multigamescreen = pGamescreen;

        int buttonHeight = (int)(pTotalWidth / (2 + ASPECT_RATIO));
        int buttonWidth = (int)(ASPECT_RATIO * buttonHeight);

        switch(figure.getFigureType())
        {
            case NUMBER:
                createNumber(pX, pY, buttonWidth, buttonHeight);
                break;
            case PLUSMINUS:
                createPlusMin(pX, pY, buttonWidth, buttonHeight);
                break;
            case EQUAL:
                createEqual(pX, pY, buttonWidth, buttonHeight);
                break;
        }
    }

    private void createNumber(int pX, int pY, int pButtonLength, int pButtonShortLength)
    {
        buttons = new JToggleButton[]
        {
            new JToggleButton(), new JToggleButton(), new JToggleButton(), new JToggleButton(), new JToggleButton(), new JToggleButton(), new JToggleButton()
        };

        buttons[0].setBounds(pX + pButtonShortLength, pY, pButtonLength, pButtonShortLength);
        buttons[1].setBounds(pX, pY + pButtonShortLength, pButtonShortLength, pButtonLength);
        buttons[2].setBounds(pX + pButtonLength + pButtonShortLength, pY + pButtonShortLength, pButtonShortLength, pButtonLength);
        buttons[3].setBounds(pX + pButtonShortLength, pY + pButtonLength + pButtonShortLength, pButtonLength, pButtonShortLength);
        buttons[4].setBounds(pX, pY + pButtonLength + (pButtonShortLength * 2), pButtonShortLength, pButtonLength);
        buttons[5].setBounds(pX + pButtonLength + pButtonShortLength, pY + pButtonLength + (pButtonShortLength * 2), pButtonShortLength, pButtonLength);
        buttons[6].setBounds(pX + pButtonShortLength, pY + (pButtonLength * 2) + (pButtonShortLength * 2), pButtonLength, pButtonShortLength);

        setButtonStyles();
        createListeners();
    }

    private void createEqual(int pX, int pY, int pButtonLength, int pButtonShortLength)
    {
        buttons = new JToggleButton[]
        {
            new JToggleButton(), new JToggleButton()
        };

        double x = pX + pButtonShortLength;
        double y = pY + 0.5 * pButtonShortLength + 1 * pButtonLength;

        buttons[0].setBounds((int)x, (int)y, pButtonLength, pButtonShortLength);
        buttons[1].setBounds((int)x, (int)(y + (1.5 * pButtonShortLength)), pButtonLength, pButtonShortLength);

        setButtonStyles();
        buttons[0].addActionListener((ActionEvent e)
                ->
                {
                    buttons[0].setSelected(true);
                });
        buttons[1].addActionListener((ActionEvent e)
                ->
                {
                    buttons[1].setSelected(true);
                });
    }

    private void createPlusMin(int pX, int pY, int pButtonLength, int pButtonShortLength)
    {
        buttons = new JToggleButton[]
        {
            new JToggleButton(), new JToggleButton()
        };

        double x = pX + pButtonShortLength;
        double y = pY + 1.5 * pButtonShortLength + 0.5 * pButtonLength;

        buttons[0].setBounds((int)x, (int)(y + (0.5 * pButtonLength) - (0.5 * pButtonShortLength)), pButtonLength, pButtonShortLength);
        buttons[1].setBounds((int)(x + (0.5 * pButtonLength) - (0.5 * pButtonShortLength)), (int)y, pButtonShortLength, pButtonLength);

        setButtonStyles();
        createListeners();
    }

    private void setButtonStyles()
    {
        for(int i = 0; i < buttons.length; i++)
        {
            buttons[i].setUI(new MetalToggleButtonUI()
            {
                @Override
                protected Color getSelectColor()
                {
                    return Color.RED;
                }
            });
            buttons[i].setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            buttons[i].setBackground(Color.BLACK);
            buttons[i].setSelected(figure.getPosition(i));
            if(gamescreen != null)
                gamescreen.add(buttons[i]);
            else
                multigamescreen.add(buttons[i]);
        }
    }

    private void createListeners()
    {
        for(JToggleButton button : buttons)
        {
            if(gamescreen != null)
                button.addActionListener(new FigureDisplayListener(this));
            else
                button.addActionListener(new FigureDisplayListenerMulti(this));
        }
    }

    public Gamescreen getGamescreen()
    {
        return gamescreen;
    }

    public MultiplayerGamesreen getMultiplayerGamesreen()
    {
        return multigamescreen;
    }

    public Figure getFigure()
    {
        return figure;
    }

    public JToggleButton[] getButtons()
    {
        return buttons;
    }
}
