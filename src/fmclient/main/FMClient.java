package fmclient.main;

import fmclient.screens.MultiplayerGamesreen;
import fmclient.screens.MultiplayerMenu;
import fmlibrary.*;
import static fmlibrary.ProtocolType.GAMEFINISHED;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;

public class FMClient extends Client
{
    private MultiplayerMenu menu;
    private MultiplayerGamesreen game;

    public FMClient(String pIPAdresse, int pPortNr, MultiplayerMenu pMenu)
    {
        super(pIPAdresse, pPortNr);
        menu = pMenu;
    }

    public void send(ProtocolType pType, String... pMessage)
    {
        super.send(Util.createMessage(pType, String.join("\n", pMessage)));
    }

    @Override
    public void processMessage(String pMessage)
    {
        ProtocolType protocolType = Util.getTypeFromMessage(pMessage);
        String message = Util.getMessageFromMessage(pMessage);

        switch(protocolType)
        {
            case USERS:
                menu.getnumberOfUsersLabel().setText(message.substring(0, message.indexOf("#")));
                message = Util.getMessageFromMessage(message);
                DefaultListModel<String> list = (DefaultListModel<String>)menu.getList().getModel();
                list.removeAllElements();
                while(!message.isEmpty())
                {
                    list.addElement(message.substring(0, message.indexOf('#')) + "\n");
                    message = Util.getMessageFromMessage(message);
                }
                break;
            case GAMESTART:
                int time = Integer.valueOf(message.substring(0, message.indexOf("#")));
                message = Util.getMessageFromMessage(message);
                List calc = GameGenerator.stringToFigures(message);
                Task task = new Task(calc, new List());
                game = new MultiplayerGamesreen(menu.getView(), this, task, time);
                menu.changeScreen(game);
                break;
            case GAME:
                List calc2 = GameGenerator.stringToFigures(message);
                Task task2 = new Task(calc2, new List());
                if(game.getLeftTime() > 0)
                {
                    MultiplayerGamesreen pGame = new MultiplayerGamesreen(menu.getView(), this, task2, game.getLeftTime());
                    game.changeScreen(pGame);
                    game = pGame;
                }
                else
                    send(GAMEFINISHED);
                break;
            case TASKLIST:
                DefaultTableModel table = (DefaultTableModel)game.getTable().getModel();
                table.setRowCount(0);
                while(!message.isEmpty())
                {
                    table.addRow(new Object[]
                    {
                        message.substring(0, message.indexOf("$")), message.substring(message.indexOf("$") + 1, message.indexOf("#"))
                    });
                    message = Util.getMessageFromMessage(message);
                }
                break;
            case GAMEFINISHED:
                game.setWinnerText(message + " won the Game");
                break;

        }
    }
}
