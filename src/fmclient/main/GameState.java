package fmclient.main;

public enum GameState
{
    NO_MATCH_LEFT,
    ONE_MATCH_LEFT,
    ONE_MATCH_FROM_LAST_FIGURE_LEFT;
}
